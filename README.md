HeadScript
==========

Adds an easy system for adding and configuring head sripts for wiki on MediaWiki powered websites from the LocalSettings.php file.


Installation
------------

Use the command `git clone https://gitlab.com/tobiaska123/mediawiki-extension-HeadScript.git HeadScript` to get the extension code.

Further installation instructions can be found at http://www.mediawiki.org/wiki/Extension:HeadScript